var playerOne = prompt("Player One: Enter your name");
var playerTwo = prompt("Player Two: Enter your name");

function restart() {
    console.log("In restart")
    var row = "<tr>", tab = "";
    for (var i = 0; i < 7; i++) {
        row += "<td><button type='button'></button></td>"
    }
    row += "</tr>"
    for (var i = 0; i < 6; i++) {
        tab += row;
    }
    $("table").html(tab)

    var playerOneColor = "rgb(86, 151, 255)"
    var playerTwoColor = "rgb(237, 45, 73)"

    var table = $("table tr");

    function reportWin(x, y) {
        console.log("game finished");
        console.log(x)
        console.log(y)
    }
    // gray = #6b6d75, red = #840030
    function changeColor(x, y, color) {
        return table.eq(x).find('td').eq(y).find('button').css("background-color", color);
    }
    function returnColor(x, y) {
        return table.eq(x).find('td').eq(y).find('button').css("background-color");
    }

    function checkBotton(y) {
        var colorReport = returnColor(5, y);
        for (var row = 5; row >= 0; row--) {
            colorReport = returnColor(row, y);
            if (colorReport === "rgb(128, 128, 128)") {
                return row;
            }
        }
    }
    function colorMatch(one, two, three, four) {
        return (one === two && one === three && one === four && one != "rgb(128, 128, 128)" && one !== undefined);
    }
    function winCheck() {
        for (var row = 0; row < 6; row++) {
            for (var col = 0; col < 4; col++) {
                if (colorMatch(returnColor(row, col), returnColor(row, col + 1), returnColor(row, col + 2), returnColor(row, col + 3))) {
                    console.log("horiziontal win");
                    return true;
                }
                else if (colorMatch(returnColor(row, col), returnColor(row - 1, col), returnColor(row - 2, col), returnColor(row - 3, col))) {
                    console.log("horiziontal win");
                    return true;
                }
                else if (colorMatch(returnColor(row, col), returnColor(row - 1, col + 1), returnColor(row - 2, col + 2), returnColor(row - 3, col + 3))) {
                    console.log("horiziontal win");

                    return true;
                }
                else {
                    continue;
                }
            }
        }
    }

    var currentPlayer = 1;
    var currentName = playerOne;
    var currentColor = playerOneColor;

    $("h1").text(playerOne + " it is your, pick a cloumn to drop in!")

    $(".board button").on("click", function () {
        var col = $(this).closest("td").index(); // find column number
        var buttonAvail = checkBotton(col);
        changeColor(buttonAvail, col, currentColor);

        if (winCheck()) {
            reportWin(row, col);
            alert("Win")
        }

        currentPlayer *= -1;
        console.log(currentPlayer);
        if (currentPlayer === 1) {
            currentName = playerOne;
            $("h1").text(currentName + " it is your turn.");
            currentColor = playerOneColor;
        }
        else {
            currentName = playerTwo;
            $("h1").text(currentName + " it is your turn");
            currentColor = playerTwoColor;
        }
    })
}
restart();
$(".btn").click(function () { restart(); console.log("Restart called") });